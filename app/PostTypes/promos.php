<?php

/*
* Creating a function to create our CPT
*/


// Creating a Promos Custom Post Type
function custom_post_type_promos() {
	$labels = array(
		'name'                => __( 'Promos' ),
		'singular_name'       => __( 'Promo'),
		'menu_name'           => __( 'Promos'),
		'parent_item_colon'   => __( 'Parent Promo'),
		'all_items'           => __( 'All Promos'),
		'view_item'           => __( 'View Promo'),
		'add_new_item'        => __( 'Add New Promo'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Promo'),
		'update_item'         => __( 'Update Promo'),
		'search_items'        => __( 'Search Promo'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'promos'),
		'description'         => __( 'Promo Details'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
        'yarpp_support'       => true,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
        'menu_icon'           => 'dashicons-products',
);
	register_post_type( 'promo', $args );
}
add_action( 'init', 'custom_post_type_promos', 0 );
