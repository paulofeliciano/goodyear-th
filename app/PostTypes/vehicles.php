<?php

/*
* Creating a function to create our CPT
*/


// Creating a Deals Custom Post Type
// function custom_post_type_vehicles() {
// 	$labels = array(
// 		'name'                => __( 'Vehicles' ),
// 		'singular_name'       => __( 'Vehicle'),
// 		'menu_name'           => __( 'Vehicles'),
// 		'parent_item_colon'   => __( 'Parent Vehicle'),
// 		'all_items'           => __( 'All Vehicles'),
// 		'view_item'           => __( 'View Size'),
// 		'add_new_item'        => __( 'Add New Vehicle'),
// 		'add_new'             => __( 'Add New'),
// 		'edit_item'           => __( 'Edit Vehicle'),
// 		'update_item'         => __( 'Update Vehicle'),
// 		'search_items'        => __( 'Search Vehicle'),
// 		'not_found'           => __( 'Not Found'),
// 		'not_found_in_trash'  => __( 'Not found in Trash')
// 	);
// 	$args = array(
// 		'label'               => __( 'vehicles'),
// 		'description'         => __( 'Vehicle Details'),
// 		'labels'              => $labels,
// 		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
// 		'public'              => true,
// 		'hierarchical'        => false,
// 		'show_ui'             => true,
// 		'show_in_menu'        => true,
// 		'show_in_nav_menus'   => true,
// 		'show_in_admin_bar'   => true,
// 		'has_archive'         => true,
// 		'can_export'          => true,
// 		'exclude_from_search' => false,
//     'yarpp_support'       => true,
// 		'taxonomies' 	      => array('post_tag'),
// 		'publicly_queryable'  => true,
// 		'capability_type'     => 'page',
//     'menu_icon'           => 'dashicons-admin-tools',
// );
// 	register_post_type( 'vehicles', $args );
// }
// add_action( 'init', 'custom_post_type_vehicles', 0 );
