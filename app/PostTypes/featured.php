<?php

/*
* Creating a function to create our CPT
*/


// Creating a Deals Custom Post Type
function custom_post_type_featured() {
	$labels = array(
		'name'                => __( 'Featured' ),
		'singular_name'       => __( 'Featured'),
		'menu_name'           => __( 'Featured'),
		'parent_item_colon'   => __( 'Parent Featured'),
		'all_items'           => __( 'All Featured'),
		'view_item'           => __( 'View Featured'),
		'add_new_item'        => __( 'Add New Feature'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Feature'),
		'update_item'         => __( 'Update Feature'),
		'search_items'        => __( 'Search Feature'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'featured'),
		'description'         => __( 'Feature Details'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
    'yarpp_support'       => true,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
    'menu_icon'           => 'dashicons-megaphone',
);
	register_post_type( 'featured', $args );
}
add_action( 'init', 'custom_post_type_featured', 0 );
