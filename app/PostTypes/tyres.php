<?php

/*
* Creating a function to create our CPT
*/


// Creating a Deals Custom Post Type
function custom_post_type_tyres() {
	$labels = array(
		'name'                => __( 'Tyres' ),
		'singular_name'       => __( 'Tyre'),
		'menu_name'           => __( 'Tyres'),
		'parent_item_colon'   => __( 'Parent Tyre'),
		'all_items'           => __( 'All Tyres'),
		'view_item'           => __( 'View Tyre'),
		'add_new_item'        => __( 'Add New Tyre'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Tyre'),
		'update_item'         => __( 'Update Tyre'),
		'search_items'        => __( 'Search Tyre'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'tyre'),
		'description'         => __( 'Tyre Details'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields','comments'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => false,
		'can_export'          => true,
		'exclude_from_search' => false,
    'yarpp_support'       => true,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
    'menu_icon'           => 'dashicons-sos',
);
	register_post_type( 'tyre', $args );
}
add_action( 'init', 'custom_post_type_tyres', 0 );



add_action( 'init', 'custom_post_taxonomy_featuers', 0 );

function custom_post_taxonomy_featuers() {
  $labels = array(
    'name' => _x( 'Features', 'taxonomy general name' ),
    'singular_name' => _x( 'Feature', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Features' ),
    'all_items' => __( 'All Features' ),
    'parent_item' => __( 'Parent Feature' ),
    'parent_item_colon' => __( 'Parent Feature:' ),
    'edit_item' => __( 'Edit Feature' ),
    'update_item' => __( 'Update Feature' ),
    'add_new_item' => __( 'Add New Feature' ),
    'new_item_name' => __( 'New Feature Name' ),
    'menu_name' => __( 'Features' ),
  );

  register_taxonomy('features',array('tyre'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'features' ),
  ));
}


add_action( 'init', 'custom_post_taxonomy_category', 0 );

function custom_post_taxonomy_category() {
  $labels = array(
    'name' => _x( 'Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Categories' ),
    'all_items' => __( 'All Categories' ),
    'parent_item' => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item' => __( 'Edit Category' ),
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add New Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'menu_name' => __( 'Categories' ),
  );

  register_taxonomy('categories',array('tyre'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'categories' ),
  ));
}


add_action( 'init', 'custom_post_taxonomy_vehicles', 0 );

function custom_post_taxonomy_vehicles() {
  $labels = array(
    'name' => _x( 'Vehicles', 'taxonomy general name' ),
    'singular_name' => _x( 'Vehicle', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Vehicles' ),
    'all_items' => __( 'All Vehicles' ),
    'parent_item' => __( 'Parent Vehicle' ),
    'parent_item_colon' => __( 'Parent Vehicle:' ),
    'edit_item' => __( 'Edit Vehicle' ),
    'update_item' => __( 'Update Vehicle' ),
    'add_new_item' => __( 'Add New Vehicle' ),
    'new_item_name' => __( 'New Vehicle Name' ),
    'menu_name' => __( 'Vehicles' ),
  );

  register_taxonomy('vehicles',array('tyre'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'vehicles' ),
  ));
}
