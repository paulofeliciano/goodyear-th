<?php

/*
* Creating a function to create our CPT
*/


// Creating Sizes Custom Post Type
function custom_post_type_sizes() {
	$labels = array(
		'name'                => __( 'Sizes' ),
		'singular_name'       => __( 'Size'),
		'menu_name'           => __( 'Sizes'),
		'parent_item_colon'   => __( 'Parent Size'),
		'all_items'           => __( 'All Sizes'),
		'view_item'           => __( 'View Size'),
		'add_new_item'        => __( 'Add New Size'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Size'),
		'update_item'         => __( 'Update Size'),
		'search_items'        => __( 'Search Size'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'sizes'),
		'description'         => __( 'Size Details'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
    'yarpp_support'       => true,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
    'menu_icon'           => 'dashicons-move',
);
	register_post_type( 'sizes', $args );
}
add_action( 'init', 'custom_post_type_sizes', 0 );
