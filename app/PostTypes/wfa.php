<?php

/*
* Creating a function to create our CPT
*/


// Creating a WFA Custom Post Type
function custom_post_type_wfa() {
	$labels = array(
		'name'                => __( 'WFA' ),	
		'singular_name'       => __( 'WFA'),
		'menu_name'           => __( 'WFA'),
		'parent_item_colon'   => __( 'Parent WFA'),
		'all_items'           => __( 'All WFA'),
		'view_item'           => __( 'View WFA'),
		'add_new_item'        => __( 'Add New WFA'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit WFA'),
		'update_item'         => __( 'Update WFA'),
		'search_items'        => __( 'Search WFA'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'wfa'),
		'description'         => __( 'WFA Details'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
    	'yarpp_support'       => true,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
   		'menu_icon'           => 'dashicons-format-status',
);
	register_post_type( 'wfa', $args );
}
add_action( 'init', 'custom_post_type_wfa', 0 );
