<?php

/*
* Creating a function to create our CPT
*/


// Creating a FAQs Custom Post Type
function custom_post_type_faqs() {
	$labels = array(
		'name'                => __( 'FAQs' ),	
		'singular_name'       => __( 'FAQs'),
		'menu_name'           => __( 'FAQs'),
		'parent_item_colon'   => __( 'Parent FAQs'),
		'all_items'           => __( 'All FAQs'),
		'view_item'           => __( 'View FAQs'),
		'add_new_item'        => __( 'Add New FAQs'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit FAQs'),
		'update_item'         => __( 'Update FAQs'),
		'search_items'        => __( 'Search FAQs'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'faqs'),
		'description'         => __( 'FAQs Details'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
    	'yarpp_support'       => true,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
   		'menu_icon'           => 'dashicons-format-status',
);
	register_post_type( 'faqs', $args );
}
add_action( 'init', 'custom_post_type_faqs', 0 );
