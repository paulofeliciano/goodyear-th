<?php

/*
* Creating a function to create our CPT
*/


// Creating a News Custom Post Type
function custom_post_type_news() {
	$labels = array(
		'name'                => __( 'News' ),	
		'singular_name'       => __( 'News'),
		'menu_name'           => __( 'News'),
		'parent_item_colon'   => __( 'Parent News'),
		'all_items'           => __( 'All News'),
		'view_item'           => __( 'View News'),
		'add_new_item'        => __( 'Add News'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit News'),
		'update_item'         => __( 'Update News'),
		'search_items'        => __( 'Search News'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'news'),
		'description'         => __( 'News Details'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
    	'yarpp_support'       => true,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
   		'menu_icon'           => 'dashicons-admin-site-alt3',
);
	register_post_type( 'news', $args );
}
add_action( 'init', 'custom_post_type_news', 0 );
