<?php

add_shortcode('display_children', function ($category) {
    extract(shortcode_atts(array(
        'id'     => $id
    ), $category));

    global $post;

    if(!$id) {
        $id = $post->ID;
    }

    $args = array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
        'post_parent'    => $id,
        'order'          => 'ASC',
        'orderby'        => 'title'
     );

    $parent = new WP_Query( $args );
    
    if ( $parent->have_posts() ) : 
        
        echo "<div class='row'>";
    
        while ( $parent->have_posts() ) : $parent->the_post(); ?>
    
            <div class="col-md-4 display-children mb-3">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <div class="featured" style="background-image:url('<?=the_post_thumbnail_url()?>');">
                         <h3 align="center"><?php the_title(); ?></h3>
                    </div>
                </a>
            </div>
    <?php
        endwhile; 
        echo "</div>";
    endif;

wp_reset_postdata(); 
wp_reset_query();

return ob_get_clean();
});
?>