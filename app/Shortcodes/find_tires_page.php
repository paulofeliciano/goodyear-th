<?php
add_shortcode('find_tires_page', function ($atts) {
    extract(shortcode_atts(array(
        'category' => '',
        'taxonomy' => '',
        'post'     => '',
    ), $atts));

    ob_start();
    $brand = get_terms([
    'parent'     => 0,
    'taxonomy' => 'vehicles',
    'hide_empty' => true,
    'orderby'   => 'name'
    ]);
    $tyres_url = site_url() . '/all-tyres';
    if($category) {
      $tyres_url = $tyres_url . '/' . $category;
    }

    if($_POST['search_by']) :
      $search = $_POST['search_by'];
    else :
      $search = null;
    endif;

    if($_POST['brand']) :
      $post_brand = get_term_by('id', $_POST['brand'],'vehicles');
      $post_model = get_term_by('id', $_POST['model'],'vehicles');
      $post_year  = get_term_by('id', $_POST['years'],'vehicles');
    endif;

    if($_POST['width']) :
      $post_width  = $_POST['width'];
      $post_aspect = $_POST['aspect'];
      $post_rim    = $_POST['rim'];
    endif;
?>
<ul class="nav nav-tabs full-width" id="search_tyres" role="tablist">
  <li class="nav-item">
    <a class="nav-link <?php if($search != 'sizes') echo 'active'; ?>" id="find-vehicle" data-toggle="tab" href="#vehicle" role="tab" aria-controls="vehicle">
      <i class="fas fa-car secondary-color"></i> ค้นหายางรถยนต์
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php if($search == 'sizes') echo 'active'; ?>" id="find-sizes" data-toggle="tab" href="#sizes" role="tab" aria-controls="sizes">
      <i class="fas fa-expand-alt secondary-color"></i> ค้นหายางตามขนา
    </a>
  </li>
</ul>


<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade <?php if($search != 'sizes') echo 'show active'; ?> container-fluid" id="vehicle" role="tabpanel" aria-labelledby="find-vehicle">
    <form action="<?=($tyres_url)?>" method="POST">
    <input type="hidden" name="search_by" value="vehicles">
    <div class="row pt-3">
      <div class="col-md-4">
        <select class="form-control" style="margin-bottom: 5px;" id="brand" name="brand" onchange="fetchModels()">
          <option disabled selected>เลือกยี่ห้อรถ</option>
          <?php
            foreach ($brand as $b) {
                echo '<option value="'.$b->term_id.'"';
                if(strtolower($post_brand->name) == strtolower($b->name)){ echo 'selected'; }
                echo '   >'.$b->name.'</option>';
            }
            ?>
        </select>
      </div>
      <div class="col-md-4">
        <select disabled class="form-control" style="margin-bottom: 5px;" id="fetch_models" name="model" onchange="fetchYears()">
          <option disabled selected>เลือกรุ่นรถ</option>
          <?php
          if($post_model):
            echo "<option selected>$post_model->name</option>";
          endif;
           ?>
        </select>
      </div>
      <div class="col-md-4">
        <select disabled class="form-control" style="margin-bottom: 5px;" id="fetch_years" name="years" onchange="activateButton()">
          <option disabled selected>เลือกปีผลิต</option>
          <?php
          if($post_year):
            echo "<option selected>$post_year->name</option>";
          endif;
           ?>
        </select>
      </div>
      <div class="col-md-12">
        <button class="btn btn-secondary py-2 px-4 mt-2 float-right" id="find_tyres_with_vehicles_button" name="searchbyvehicle" disabled>ค้นหา</button>
      </div>
    </div>
  </form>
  </div>

  <div class="tab-pane fade container-fluid <?php if($search == 'sizes') echo 'show active'; ?>" id="sizes" role="tabpanel" aria-labelledby="profile-tab">
    <form action="<?=($tyres_url)?>" method="POST">
      <div class="row mt-3">
        <div class="col-md-4">
          <input type="hidden" name="search_by" value="sizes">
          <select class="form-control" style="margin-bottom: 5px;" id="width" name="width" onchange="fetchAspect()">
            <option disabled selected>ความกว้าง (มม.)</option>
            <?php
            // GET WIDTH
            $args = array (
                'post_type'      => 'sizes',
                'posts_per_page' => -1,
                'orderby'        => 'meta_value_num',
                'order'          => 'ASC',
                'meta_query'     => array(
                                       array('key'     => 'width',
                                             'compare' => 'EXISTS'
                                       )
                                    )
            );

            $widths = new WP_Query($args);
            $duplicate = array();
            while ($widths->have_posts()): $widths->the_post();
                if(!in_array(get_field('width'), $duplicate)){
                  array_push($duplicate, get_field('width'));
                  echo '<option ';
                  if($post_width == get_field('width')) echo 'selected';
                  echo '>'.get_field('width').'</option>';
                }
            endwhile;
            wp_reset_query();
            ?>
          </select>
        </div>
        <div class="col-md-4">
          <select disabled class="form-control" style="margin-bottom: 5px;" id="aspect" name="aspect" onchange="fetchRim()">
            <option disabled selected>อัตราส่วนความสูงแก้มยาง</option>
            <?php
            if($post_aspect):
              echo "<option selected>$post_aspect</option>";
            endif;
             ?>
          </select>
        </div>
        <div class="col-md-4">
          <select disabled class="form-control" style="margin-bottom: 5px;" id="rim" name="rim" onchange="activateButton()">
            <option disabled selected>เส้นผ่าศูนย์กลางกระทะล้อ</option>
            <?php
            if($post_rim):
              echo "<option selected>$post_rim</option>";
            endif;
             ?>
          </select>
        </div>

      </div>
        <button class="btn btn-secondary py-2 px-4 mt-2 mb-4 float-right" id="find_tyres_with_sizes_button" name="searchbysize" disabled>ค้นหา</button>
    </form>
  </div>
</div>
<?php
    return ob_get_clean();
});
