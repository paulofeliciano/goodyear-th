<?php

add_shortcode('faqs_list', function ($category) {
    extract(shortcode_atts(array(
        'post'     => ''
    ), $category));

    $loop = new WP_Query([
        'posts_per_page' => 6,
        'post_type'      => $post,
    ]);

    $posts = $loop->posts;

    global $post;

    ob_start(); ?>
        <?php
        if(count($posts) == 0):
            echo "<h1>No result found.</h1>";
        else:
                echo "<div class='accordion goodyear-accordion' id='faqs'>";
                foreach ($posts as $i => $post) {
                    setup_postdata($post);
                    ?>
                    
                        <div class="card">
                            <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#<?="question".$post->ID?>" aria-expanded="true" aria-controls="<?="question".$post->ID?>">
                                <?=strtoupper(the_title())?>
                                </button>
                            </h2>
                            </div>

                            <div id="<?="question".$post->ID?>" class="collapse <?php if($i == 0) echo 'show'; ?>" data-parent="#faqs">
                            <div class="card-body">
                                <?=the_content()?>
                            </div>
                            </div>
                        </div>
<?php
                }
                echo "</div>";
         endif;
    wp_reset_postdata();
    wp_reset_query();
    return ob_get_clean();
});
