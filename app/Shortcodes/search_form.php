<?php

add_shortcode('search_form', function ($atts) {
    extract(shortcode_atts(array(
        'category' => $category,
        'post'     => $post,
    ), $atts));

    ob_start(); ?>

      <div class="input-group search-group">
        <input type="search" class="form-control" aria-label="Search" id="keyword" class="form-control" placeholder="Search Tyres">
        <div class="input-group-append">
        <button type="button" class="btn btn-secondary" onclick="fetch()">SEARCH</button>
        </div>
      </div>
      <input type="hidden" id="posttype" value="<?=($post)?>">
      <input type="hidden" id="categorytype" value="<?=($category)?>">
      <!-- <input type="hidden" name="parent_page_id" id="parent_page_id" value="<?php the_ID(); ?>" /> -->
      <div id="loader-img" style="margin:0 auto; display: none; text-align: center;" class="spinner-grow text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
      <div id="datafetch" style="display: none;">

      </div>
    <?php
    return ob_get_clean();
});
