<?php

add_shortcode('sidebar', function ($atts) {
    extract(shortcode_atts(array(
    'category' => $category,
    'post'     => $post,
), $atts));

    ob_start();
    global $post;
    $subpages = wp_list_pages(array(
   'echo'=>0,
   'title_li'=>'',
   'depth'=>2,
   'child_of'=> ($post->post_parent == 0 ? $post->ID : $post->post_parent)
));
    if (!empty($subpages)) {
        echo '<ul class="custom-sidebar">';
        echo $subpages;
        echo '</ul>';
    } else {
        echo 'no subpages';
    } ?>
<ul>

</ul>
<?php
    return ob_get_clean();
});
