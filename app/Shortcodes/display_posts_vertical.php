<?php

add_shortcode('display_posts_vertical', function ($category) {
    extract(shortcode_atts(array(
        'category' => '',
        'taxonomy' => '',
        'post'     => ''
    ), $category));

    if (get_query_var('paged')) {
        $paged = get_query_var('paged');
    } elseif (get_query_var('page')) {
        $paged = get_query_var('page');
    } else {
        $paged = 1;
    }

    $loop = new WP_Query([
        'posts_per_page' => 6,
        'post_type'      => $post,
        'paged'          => $paged,
        'tax_query'     => array(
        $byCategory
        ),
    ]);

    $posts = $loop->posts;

    global $post;

    ob_start(); ?>

    <div class='container my-5'>
      <div class="row">
        <?php
        if(count($posts) == 0):
            echo "<h1>No result found.</h1>";
        else:
                foreach ($posts as $i => $post) {
                    setup_postdata($post);

                    $type = 'latest-news';
                    $link = get_post_permalink(); ?>
                    <div class="col-md-12">
                    <article class="mb-3 post-<?=get_the_ID()?> <?=($type)?>">
                            <a href="<?=get_the_permalink()?>">
                                <?=(the_post_thumbnail('full', ['class' => 'img-fluid w-100']))?>
                            </a>

                        <div class="pt-3 pb-5">
                        <div class="content" style="min-height: 5em;">
                            <h3>
                                <a href="<?=get_the_permalink()?>" class="color-secondary-dark"><?=(get_the_title())?></a>
                            </h3>
                        </div>

                        <div class="features" style="min-height: 5em;">
                            <?php
                            $terms = wp_get_post_terms($post->ID, 'features');
                            $count = 0;
                            foreach ($terms as $term) {
                                ++$count;
                                if ($count < 4) {
                                    echo "<img src='".get_field('feature_image', $term)['url']."' style='width: 25px;' class='mr-1'>";
                                    echo $term->name . "<br>";
                                }
                            } ?>
                        </div>

                        <?php if (!empty($link[0])) : ?>
                        <!-- <div class="more mt-2">
                            <a href="<?=get_the_permalink()?>">
                                <button type="button" class="btn btn-secondary btn-block">MORE DETAILS</button>
                            </a>
                        </div> -->
                        <?php endif; ?>
                        </div>
                    </article>
                </div>

                    <?php
                }
         endif;
    wp_reset_postdata(); ?>
      </div>
    </div>

    <?php
    echo "<div class='container'>";
    wp_pagenavi(array( 'query' => $loop ));
    echo "</div>";


    wp_reset_query();

    return ob_get_clean();
});
