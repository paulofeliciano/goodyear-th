<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);


function header_number_widget()
{
    register_sidebar(array(
    'name' => __('Header Phone Number', 'smallenvelop'),
    'id' => 'header-number',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
));
}
add_action('widgets_init', 'header_number_widget');

function header_social_media()
{
    register_sidebar(array(
    'name' => __('Header Social Link', 'smallenvelop'),
    'id' => 'header-social',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
));
}
add_action('widgets_init', 'header_social_media');


function footer_promo_widget()
{
    register_sidebar(array(
    'name' => __('Footer Copyright', 'smallenvelop'),
    'id' => 'footer-promo',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
));
}
add_action('widgets_init', 'footer_promo_widget');


function footer_second_widget()
{
    register_sidebar(array(
    'name' => __('Footer Second', 'smallenvelop'),
    'id' => 'footer-second',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
));
}
add_action('widgets_init', 'footer_second_widget');

function footer_third_widget()
{
    register_sidebar(array(
    'name' => __('Footer Third', 'smallenvelop'),
    'id' => 'footer-third',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
));
}
add_action('widgets_init', 'footer_third_widget');

function footer_fourth_widget()
{
    register_sidebar(array(
    'name' => __('Footer Fourth', 'smallenvelop'),
    'id' => 'footer-fourth',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
));
}
add_action('widgets_init', 'footer_fourth_widget');

function footer_fifth_widget()
{
    register_sidebar(array(
    'name' => __('Footer Fifth', 'smallenvelop'),
    'id' => 'footer-fifth',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
));
}
add_action('widgets_init', 'footer_fifth_widget');

function contact_us_tyre()
{
    register_sidebar(array(
    'name' => __('Contact Us Sidebar on Tyre Page', 'smallenvelop'),
    'id' => 'contact_us_tyre',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
}
add_action('widgets_init', 'contact_us_tyre');

// ACF CUSTOM LOAD FIELD CHOICES
function acf_load_stores_field_choices($field)
{
    $loop = new WP_Query([
        'post_type'      => 'store',
    ]);
    $posts = $loop->posts;
    global $post;

    $stores = array();
    foreach ($posts as $i => $post) {
        array_push($stores, $post->post_title);
    }

    $field['choices'] = $stores;
    $choices = get_field('stores', 'option', false);
    $choices = explode("\n", $choices);
    $choices = array_map('trim', $choices);

    if (is_array($choices)) {
        foreach ($choices as $choice) {
            if ($choice != '') {
                $field['choices'][ $choice ] = $choice;
            }
        }
    }

    return $field;
}

add_filter('acf/load_field/name=stores', 'acf_load_stores_field_choices');

// function acf_load_sizes_field_choices($field)
// {
//     $loop = new WP_Query([
//         'post_type'      => 'sizes',
//     ]);
//     $posts = $loop->posts;
//     global $post;
//
//     $sizes = array();
//     foreach ($posts as $i => $post) {
//         array_push($sizes, $post->post_title);
//     }
//
//     $field['choices'] = $sizes;
//     $choices = get_field('sizes', 'option', false);
//     $choices = explode("\n", $choices);
//     $choices = array_map('trim', $choices);
//
//     if (is_array($choices)) {
//         foreach ($choices as $choice) {
//             if ($choice != '') {
//                 $field['choices'][ $choice ] = $choice;
//             }
//         }
//     }
//
//     return $field;
// }
//
// add_filter('acf/load_field/name=sizes', 'acf_load_sizes_field_choices');


add_action('wp_footer', 'ajax_fetch');
function ajax_fetch()
{
    ?>
   <script type="text/javascript">
   function fetch(){
           jQuery('#datafetch').toggle('display','block');
           jQuery('#loader-img').toggle();
           jQuery.ajax({
               url: '<?php echo admin_url('admin-ajax.php'); ?>',
               type: 'POST',
               data: {
               action: 'data_fetch',
               keyword: jQuery('#keyword').val(),
               posttype: jQuery('#posttype').val(),
               categorytype: jQuery('#categorytype').val(),
               parent_page_id: jQuery( '#parent_page_id' ).val()
               },
               success: function(data) {
                   jQuery('#datafetch').html( data );
                   jQuery('#loader-img').toggle();
               }
           });
   }
       </script>
   <?php
}

add_action('wp_ajax_data_fetch', 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch', 'data_fetch');
function data_fetch()
{
    $keyword = esc_attr($_POST['keyword']);
    if ($keyword) {
        $the_query = new WP_Query(
            array(
               'post_status'     => 'publish',
               'posts_per_page' => 3,
               's'              => $keyword,
               'post_type'      => $_POST['posttype'],
               'category'       => $_POST['categorytype']
               )
        );

        if ($the_query->have_posts()) :
           while ($the_query->have_posts()): $the_query->the_post(); ?>
               <h3><a style="padding: 0px; padding-top: 10px;" href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h3>
               <small style="color: black;" class="the-excerpt"><?php the_excerpt(); ?></small>
           <?php
           endwhile;
        wp_reset_query();
        endif;
    }
    wp_die();
}

// SEARCH TYRES BY VEHICLE
// FETCH MODELS
add_action('wp_footer', 'tyre_search');
function tyre_search()
{
    ?>
   <script type="text/javascript">
   function fetchModels(){
           jQuery('#fetch_models').prop( "disabled", true );
           jQuery('#fetch_models').append( new Option('Loading..','0',false,true) );
           jQuery('#fetch_years').prop( "disabled", true );
           jQuery('#fetch_years').val( "0" );
           jQuery('#search_tyres').prop( "disabled", true );
           jQuery('#find_tyres_with_vehicles_button').prop( "disabled", true );

           jQuery.ajax({
               url: '<?php echo admin_url('admin-ajax.php'); ?>',
               type: 'POST',
               data: {
               action: 'fetch_model',
               brand: jQuery('#brand').val()
               },
               success: function(data) {
                   jQuery('#fetch_models').prop( "disabled", false );
                   jQuery('#fetch_models').html( data );
               }
           });
   }
       </script>
   <?php
}

add_action('wp_ajax_fetch_model', 'fetch_model');
add_action('wp_ajax_nopriv_fetch_model', 'fetch_model');
function fetch_model()
{
    $brand = esc_attr($_POST['brand']);
    if ($brand) {
        $models = get_terms(
            'vehicles',
            array(
                 'parent' => $brand,
                 'hide_empty' => true,
             )
        );
        echo '<option disabled selected>Select Model</option>';
        foreach ($models as $model) {
            echo '<option value="'.$model->term_id.'">'.$model->name.'</option>';
        }
        wp_reset_query();
    }
    wp_die();
}

// FETCH YEARS
add_action('wp_footer', 'year_search');
function year_search()
{
    ?>
   <script type="text/javascript">
   function fetchYears(){
           jQuery('#fetch_years').append( new Option('Loading..','0',false,true) );

           jQuery.ajax({
               url: '<?php echo admin_url('admin-ajax.php'); ?>',
               type: 'POST',
               data: {
               action: 'fetch_year',
               model: jQuery('#fetch_models').val()
               },
               success: function(data) {
                   jQuery('#fetch_years').prop( "disabled", false );
                   jQuery('#fetch_years').html( data );
               }
           });
   }
       </script>
   <?php
}

add_action('wp_ajax_fetch_year', 'fetch_year');
add_action('wp_ajax_nopriv_fetch_year', 'fetch_year');
function fetch_year()
{
    $model = esc_attr($_POST['model']);
    if ($model) {
        $years = get_terms(
            'vehicles',
            array(
               'parent' => $model,
               'hide_empty' => true,
           )
        );
        echo '<option disabled selected>Select Year</option>';
        foreach ($years as $year) {
            echo '<option value="'.$year->term_id.'">'.$year->name.'</option>';
        }
        wp_reset_query();
    }
    wp_die();
}

// ACTIVATE SEARCH BUTTON ON FIND TYRES BY VEHICLE
add_action('wp_footer', 'search_button');
function search_button()
{
    ?>
   <script type="text/javascript">
   function activateButton(){
          if(jQuery('#fetch_years').val()){
            jQuery('#find_tyres_with_vehicles_button').prop( "disabled", false );
          }
          if(jQuery('#rim').val()){
            jQuery('#find_tyres_with_sizes_button').prop( "disabled", false );
          }
   }
       </script>
   <?php
}
// SEARCH TYRES BY VEHICLE


// SEARCH TYRES BY SIZE
// FETCH ASPECT RATIO
add_action('wp_footer', 'aspect_search');
function aspect_search()
{
    ?>
   <script type="text/javascript">
   function fetchAspect(){
           jQuery('#aspect').append( new Option('Loading..','0',false,true) );
           jQuery('#aspect').prop( "disabled", true );
           jQuery('#rim').prop( "disabled", true );
           jQuery('#rim').val( "0" );
           jQuery('#find_tyres_with_sizes_button').prop( "disabled", true );

           jQuery.ajax({
               url: '<?php echo admin_url('admin-ajax.php'); ?>',
               type: 'POST',
               data: {
               action: 'fetch_aspect',
               width: jQuery('#width').val()
               },
               success: function(data) {
                   jQuery('#aspect').prop( "disabled", false );
                   jQuery('#aspect').html( data );
               }
           });
   }
       </script>
   <?php
}

add_action('wp_ajax_fetch_aspect', 'fetch_aspect');
add_action('wp_ajax_nopriv_fetch_aspect', 'fetch_aspect');
function fetch_aspect()
{
    $width = esc_attr($_POST['width']);
    if ($width) {
        $aspects =  get_posts(array(
              'numberposts'	=> -1,
              'post_type'   => 'sizes',
              'hide_empty'  => true,
              'meta_key'	=> 'width',
              'meta_value'	=> $width
            ));
        $used = array();
        foreach ($aspects as $aspect) {
            if(!in_array(get_field('aspect_ratio', $aspect->ID), $used)):
                array_push($used, get_field('aspect_ratio', $aspect->ID));
            endif;
        }
        sort($used);
        foreach($used as $use) {
            echo "<option>". $use ."</option>";
        }

        echo "<option selected disabled>Aspect</option>";
        wp_reset_query();
    }
    wp_die();
}

//FETCH RIM
add_action('wp_footer', 'rim_search');
function rim_search()
{
    ?>
   <script type="text/javascript">
   function fetchRim(){
           jQuery('#rim').append( new Option('Loading..','0',false,true) );

           jQuery.ajax({
               url: '<?php echo admin_url('admin-ajax.php'); ?>',
               type: 'POST',
               data: {
               action: 'fetch_rim',
               width: jQuery('#width').val(),
               aspect: jQuery('#aspect').val()
               },
               success: function(data) {
                   jQuery('#rim').prop( "disabled", false );
                   jQuery('#rim').html( data );
               }
           });
   }
       </script>
   <?php
}

add_action('wp_ajax_fetch_rim', 'fetch_rim');
add_action('wp_ajax_nopriv_fetch_rim', 'fetch_rim');
function fetch_rim()
{
    $width  = esc_attr($_POST['width']);
    $aspect = esc_attr($_POST['aspect']);

    if ($aspect) {
        $rims =  get_posts(array(
              'numberposts'	=> -1,
              'post_type'		=> 'sizes',
              'hide_empty' => true,
              'meta_query' => array(
                    array(
                       'key'     => 'width',
                       'value'   => $width,
                    ),
                    array(
                       'key'     => 'aspect_ratio',
                       'value'   => $aspect,
                    )
                 )
            ));

        $used = array();
        foreach ($rims as $rim) {
            if(!in_array(get_field('rim_diameter', $rim->ID), $used)):
             array_push($used, get_field('rim_diameter', $rim->ID));
            endif;
        }
        sort($used);
        foreach($used as $use) {
            echo "<option>". $use ."</option>";
        }
        echo "<option selected disabled>Rim</option>";
        wp_reset_query();
    }
    wp_die();
}

//SEARCH TYRES BY SIZE

// FETCH SPECIFICATIONS
add_action('wp_footer', 'get_specs');
function get_specs()
{
    ?>
   <script type="text/javascript">
   function changeSpecs(){
           jQuery.ajax({
               url: '<?php echo admin_url('admin-ajax.php'); ?>',
               type: 'POST',
               data: {
               action: 'fetch_specs',
               front: jQuery('[name=front-size]').val(),
               rear: jQuery('[name=rear-size]').val()
               },
               success: function(data) {
                   jQuery('#specs').html( data );
               }
           });
   }
       </script>
   <?php
}

add_action('wp_ajax_fetch_specs', 'fetch_specs');
add_action('wp_ajax_nopriv_fetch_specs', 'fetch_specs');
function fetch_specs()
{
    $front = esc_attr($_POST['front']);
    $front = explode(' ',trim($front));
    $front = $front[0];

    $rear  = esc_attr($_POST['rear']);
    $rear = explode(' ',trim($rear));
    $rear = $rear[0];

    if ($front && $rear) {
        $args_front = array("post_type" => "sizes", "s" => $front);
        $front_query = get_posts($args_front);
        $args_rear = array("post_type" => "sizes", "s" => $rear);
        $rear_query = get_posts($args_rear);

        $infos = array('width',
                  'aspect_ratio','rim_diameter',
                  'speed_ratio', 'load_index',
                  'sidewall'
                );
        $labels = array('ความกว้าง (มม.)',
                  'อัตราส่วนความสูงแก้มยาง (%)','เส้นผ่าศูนย์กลางกระทะล้อ (นิ้ว)',
                  'Speed Ratio', 'Load Index',
                  'แก้มยาง'
                ); ?>
        <table class="table table-striped">
          <th></th>
          <th>Front Size</th>
          <th>Rear Size</th>

          <?php
          foreach ($labels as $key => $label) {
              echo "<tr><td>$label</td>";
              echo "<td>". get_field($infos[$key], $front_query[0]->ID) . "</td>";
              echo "<td>". get_field($infos[$key], $rear_query[0]->ID) . "</td></tr>";
          } ?>

      </table>
        <?php
        wp_reset_query();
    }
    wp_die();
}
// FETCH SPECIFICATIONS

add_action('wp_footer', 'magnify');
function magnify()
{
    ?>
  <script type="text/javascript">
  function magnify(imgID, zoom) {
  var img, glass, w, h, bw;
  img = document.getElementById(imgID);
  // console.log(img);

  /* Create magnifier glass: */
  glass = document.createElement("DIV");
  glass.setAttribute("class", "img-magnifier-glass");

  /* Insert magnifier glass: */
  img.parentElement.insertBefore(glass, img);

  /* Set background properties for the magnifier glass: */
  glass.style.backgroundImage = "url('" + img.src + "')";
  glass.style.backgroundRepeat = "no-repeat";
  glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
  bw = 3;
  w = glass.offsetWidth / 2;
  h = glass.offsetHeight / 2;

  /* Execute a function when someone moves the magnifier glass over the image: */
  glass.addEventListener("mousemove", moveMagnifier);
  img.addEventListener("mousemove", moveMagnifier);

  /*and also for touch screens:*/
  glass.addEventListener("touchmove", moveMagnifier);
  img.addEventListener("touchmove", moveMagnifier);
  function moveMagnifier(e) {
    var pos, x, y;
    /* Prevent any other actions that may occur when moving over the image */
    e.preventDefault();
    /* Get the cursor's x and y positions: */
    pos = getCursorPos(e);
    x = pos.x;
    y = pos.y;
    /* Prevent the magnifier glass from being positioned outside the image: */
    if (x > img.width - (w / zoom)) {x = img.width - (w / zoom);}
    if (x < w / zoom) {x = w / zoom;}
    if (y > img.height - (h / zoom)) {y = img.height - (h / zoom);}
    if (y < h / zoom) {y = h / zoom;}
    /* Set the position of the magnifier glass: */
    glass.style.left = (x - w) + "px";
    glass.style.top = (y - h) + "px";
    /* Display what the magnifier glass "sees": */
    glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
  }

  function getCursorPos(e) {
    var a, x = 0, y = 0;
    e = e || window.event;
    /* Get the x and y positions of the image: */
    a = img.getBoundingClientRect();
    /* Calculate the cursor's x and y coordinates, relative to the image: */
    x = e.pageX - a.left;
    y = e.pageY - a.top;
    /* Consider any page scrolling: */
    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return {x : x, y : y};
  }
}
  </script>
  <?php
}


// MULTIPLE POST THUMBNAILS
// if (class_exists('MultiPostThumbnails')) {
//     new MultiPostThumbnails(
//         array(
//             // Replace [YOUR THEME TEXT DOMAIN] below with the text domain of your theme (found in the theme's `style.css`).
//             'label' => __( 'Second Image', 'sage'),
//             'id' => '2',
//             'post_type' => 'tyre'
//         )
//     );
//
//     new MultiPostThumbnails(
//         array(
//             // Replace [YOUR THEME TEXT DOMAIN] below with the text domain of your theme (found in the theme's `style.css`).
//             'label' => __( 'Third Image', 'sage'),
//             'id' => '3',
//             'post_type' => 'tyre'
//         )
//     );
//
// }



// // Extend Search
// function search_filter($query) {
//     if ( !is_admin() && $query->is_main_query() ) {
//         if ( $query->is_search ) {
//
//             $tags = $_GET['tags'];
//             if ( isset( $_GET['tags'] ) && $_GET['tags'] == 'Etiqueta' ) {
//                 $tags = NULL;
//             } elseif ( $tags != NULL ) {
//                 $query->set( 'tag', $tags );
//             }
//
//             $autor = $_GET['autor'];
//             if ( isset( $_GET['autor'] ) && $_GET['autor'] == 'Autor' ) {
//                 $autor = NULL;
//             } elseif ( $autor != NULL ) {
//                 $metaQuery = array( array( 'key' => 'autor', 'value' => $autor, 'compare' => 'LIKE' ) );
//                 $query->set( 'meta_query' , $metaQuery );
//             }
//
// }   }   }
// add_action('pre_get_posts','search_filter');

function wpd_news_query( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'news' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', 3 );
    }
}
add_action( 'pre_get_posts', 'wpd_news_query' );

function wpd_promo_query( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'promo' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', 3 );
    }
}
add_action( 'pre_get_posts', 'wpd_promo_query' );

// PRE GET POSTS
function tyre_archive_query($query)
{
    if (! is_admin()
        && $query->is_post_type_archive('tyre')
        && $query->is_main_query()) {
        $query->set('posts_per_page', 8);
    }
}
add_action('pre_get_posts', 'tyre_archive_query');

function custom_post_type_cat_filter($query)
{
    if (!is_admin() && $query->is_main_query()) {
        if ($query->is_category()) {
            $query->set('post_type', array( 'post', 'tyre' ));
        }
    }
}

add_action('pre_get_posts', 'custom_post_type_cat_filter');
// PRE GET POSTS


add_filter( 'wp_nav_menu_objects', 'add_has_children_to_nav_items' );
function add_has_children_to_nav_items( $items )
{
    $parents = wp_list_pluck( $items, 'menu_item_parent');
    foreach ( $items as $item )
        in_array( $item->ID, $parents ) && $item->classes[] = 'has-children';
    return $items;
}


function has_children() {
    global $post;

    $children = get_pages( array( 'child_of' => $post->ID ) );
    if( count( $children ) == 0 ) {
        return false;
    } else {
        return true;
    }
}