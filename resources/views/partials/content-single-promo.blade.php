<article @php post_class() @endphp>
  <header>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    {!! get_the_date() !!}
  </header>
  <div class="row my-4">
    <div class="col-md-6 mb-3">
      <div align="center">
      @php
       the_post_thumbnail('full', ['class' => 'img-fluid w-100'])
      @endphp
      @php comments_template('/partials/comments.blade.php') @endphp

      </div>
    </div>
    <div class="col-md-6 mb-3">
      <div class="entry-content">
      @php the_content() @endphp
      </div>
    </div>
  </div>
</article>
