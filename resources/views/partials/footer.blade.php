<footer>
  <div id="footer-featured">
    <div class="row container-fluid">
      <?php
        wp_reset_postdata();
        $loop = new WP_Query([
            'posts_per_page' => 3,
            'post_type'      => 'featured'
        ]);
        $posts = $loop->posts;
        global $post;
        foreach($posts as $post) {
          setup_postdata($post);
          echo "<div class='col-md-4 hide-on-mobile'>";
            echo "<div class='row'>";
            echo "<div class='col-md-6'><img style='max-width: 220px;' src='".get_the_post_thumbnail_url()."'></div>";
            echo "<div class='col-md-6'><h4>". get_the_title()."</h4>";
            echo "<p>". get_the_excerpt() ."</p>";
            $label = get_field('url_label', $post->ID);
            $url   = get_field('url', $post->ID);
            echo "<a href='$url' class='secondary-color'>$label <i class='fa fa-chevron-right fa-xs'></i></a></div>";
            echo "</div></div>";
        }
      ?>
      <div class="owl-carousel owl-theme mobile-only">
        <?php
        foreach($posts as $post) {
          setup_postdata($post);
          echo "<div class='col-md-4'>";
          echo "<img src='".get_the_post_thumbnail_url()."'>";
          echo "<h4>". get_the_title()."</h4>";
          echo "<p>". get_the_excerpt() ."</p>";
          $label = get_field('url_label', $post->ID);
          $url = get_field('url', $post->ID);
          echo "<a href='$url' class='secondary-color'>$label <i class='fa fa-chevron-right fa-xs'></i></a>";
          echo "</div>";
        }
        wp_reset_postdata();
        ?>
      </div>
    </div>
  </div>
  <!-- <div class="container"> -->
  <div class="row container-fluid">
    <div class="col">
      <?php
       dynamic_sidebar('sidebar-footer');
       ?>
    </div>
    <div class="col">
      <?php
       dynamic_sidebar('footer-second');
       ?>
    </div>
    <div class="col">
      <?php
       dynamic_sidebar('footer-third');
       ?>
    </div>
    <div class="col">
      <?php
       dynamic_sidebar('footer-fourth');
       ?>
    </div>
    <div class="col">
      <?php
       dynamic_sidebar('footer-fifth');
       ?>
    </div>
  </div>


  <!-- </div> -->
</footer>
<div id="promo-footer">
  <div class="container-fluid py-3">
    <?php
     dynamic_sidebar('footer-promo');
    ?>
  </div>
</div>

<script>
  jQuery(document).ready(function() {

    jQuery('.owl-carousel').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      responsive: {
        0: {
          items: 1,
          nav: false
        }
      }
    })

  });
</script>
