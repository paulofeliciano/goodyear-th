<header class="banner">
<?php
global $wp;
$current_url = home_url( add_query_arg( array(), $wp->request ) ) . "/";
?>
  <div id="promo-header">
    <div class="row container">
      <a href="<?=site_url() . '/store-locator'?>">
        <span class="mr-3">ค้นหาสาขาใกล้เคียง</span>
        <span><i class="fas fa-map-marker-alt"></i> ค้นหาศูนย์บริการ</span>
      </a>
    </div>
  </div>

  <nav id="primary-navigation" class="navbar navbar-expand-lg navbar-light">
    <?php
    if ( has_custom_logo() ) : 
        the_custom_logo(); 
    else :
    ?>
    <a href="<?php echo get_home_url(); ?>" title="<?php echo esc_attr( get_bloginfo('name' ) ); ?>">
      <img class="py-2" src="@asset('images/gyac_logo.png')" style="max-width: 169px;"
      alt="<?php echo esc_attr( get_bloginfo('name' ) ); ?>">
    </a>
    <?php endif; ?>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars" style="color: #fff;"></i>
    </button>

    <ul class="nav navbar-nav ml-auto hide-on-mobile">
      <li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-5">
        <a aria-current="page">
          <h4 class="call-us"><?php dynamic_sidebar('header-number'); ?></h4>
        </a>
      </li>
      <li>
          <h4><?php dynamic_sidebar('header-social'); ?></h4>
      </li>
      <li id="menu-item-6" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-6">

        <a>
          <h4>
            <i style="color: white;" class="fa fa-search" aria-hidden="true"></i>
            <div class="search-box">
              <?php
                  echo do_shortcode("[search_form post='tyre']");
               ?>
            </div>
          </h4>
        </a>

      </li>
    </ul>

  </nav>


  <nav class="navbar navbar-expand-lg" id="bottom-header">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <?php
        $menuLocations = get_nav_menu_locations();
        $menuID = $menuLocations['primary_navigation'];
        $primaryNav = add_has_children_to_nav_items(wp_get_nav_menu_items($menuID));
      ?>

      <ul class="navbar-nav centered-nav">
        <?php
          foreach($primaryNav as $nav) :
            if($nav->menu_item_parent == 0 && in_array('has-children', $nav->classes)) :
                  echo "<li class='nav-item dropdown position-static ";
                  if($current_url == $nav->url) echo 'current-menu-item';
                  echo "'>";
                  echo "<a class='nav-link' href='$nav->url'>$nav->title</a>";
                  echo "<div class='dropdown-menu w-100 hide-on-mobile' aria-labelledby='navbarDropdown'>";
                  echo "<div class='row container-fluid'>";
                  echo "<div class='col-md-12'><h2>".strtoupper($nav->title)."</h2><hr></div>";
                  foreach($primaryNav as $child) :
                       if($nav->ID == $child->menu_item_parent) :
                       echo "<div class='col'><h5><strong>";
                       echo "<a href='$child->url'>". strtoupper($child->title) . "<hr></a></strong></h5>";
                       echo "<p>$child->description</p>";
                       echo "</div>";
                      endif;
                  endforeach;
                  echo "</div></div>";
                  echo "</li>";
            elseif($nav->menu_item_parent == 0 && !in_array('has-children', $nav->classes)) :
                  echo "<li class='nav-item ";
                  if($current_url == $nav->url) echo 'current-menu-item';
                  echo "'>";
                  echo "<a class='nav-link' href='$nav->url'>$nav->title</a>";
                  echo "</li>";
            endif;
          endforeach;
        ?>
      </ul>
    </div>
  </nav>

  <div class="container-fluid mobile-only py-3" id="numandsearch-mobile">
    <h4 class="float-right mr-3"><?php dynamic_sidebar('header-social'); ?></h4>
    <h4 class="call-us"><?php dynamic_sidebar('header-number'); ?></h4>
  </div>

</header>
<?php
global $wp;
if(!is_front_page() && !is_archive() && !strpos($current_url, 'all-tyres'))   :
  global $post;
?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>"><i class="fas fa-home"></i></a></li>
    <?php if($post->post_parent): ?>
    <li class="breadcrumb-item"><a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title($post->post_parent); ?></a></li>
    <?php endif; ?>
    <li class="breadcrumb-item active" aria-current="page"><?php echo $post->post_title; ?></li>
  </ol>
</nav>
<?php endif;
?>



<script>
  jQuery(document).ready(function($) {
    $(".fa-search").click(function() {
      $(".search-box").toggle();
      $("input[type='text']").focus();
    });
  });
</script>
