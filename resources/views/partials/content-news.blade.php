<div class="col-md-4 mb-3">
  <a href="<?=get_the_permalink()?>">
    <div class="archive-list-featured-image mb-3" style="background: url('<?=wp_get_attachment_url( get_post_thumbnail_id($post_id) )?>');">
    </div>
  </a>
</div>
<div class="col-md-8 mb-3">
  <article class="post-<?=get_the_ID()?> <?=($type)?>">

    <div class="px-2">
      <div class="content" style="min-height: 5em;">
        <h3>
          <a href="<?=get_the_permalink()?>"><?=(get_the_title())?></a>
        </h3>
        <span class="hide-continued">
          <?=the_excerpt()?>
        </span>
      </div>
    </div>
  </article>
</div>
