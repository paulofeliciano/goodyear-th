<div class="accordion mt-3 mb-5" id="accordionExample">
  {{-- @if(comments_open())
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        เขียนความคิดเห็น
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        @php comments_template('/partials/comments.blade.php')
        @endphp
      </div>
    </div>
  </div>
  @endif --}}

  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        ข้อมูลผลิตภัณฑ์
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        @php the_content()
        @endphp
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        รายละเอียดทางเทคนิค
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div id="specs" onload="changeSpecs();">
        </div>
        <p><a href="<?=(get_home_url() . '/glossary-of-tyre-terminologies/')?>">View Tyre Glossary</a></p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
        การรับประกัน
        </button>
      </h2>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">
          @php
          if(get_field('wfa',$post->ID) == 1): @endphp
          <img src="@asset('images/wfa.png')" width="200">
          @php
          endif;

          if(get_field('rof',$post->ID) == 1): @endphp
          <img src="@asset('images/rof.png')" width="200">
          @php
            endif;
          @endphp
      </div>
    </div>
  </div>
</div>
