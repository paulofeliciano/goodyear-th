<article @php post_class() @endphp>
  <header>

  </header>
  <?php
    global $wp;
    $current_url = home_url(add_query_arg(array(), $wp->request));
  ?>

  <div class="entry-content">
    <div class="row">
      <div class="col-md-7">
        <h1 class="entry-title">{!! get_the_title() !!}</h1>
        <?php
        $terms = wp_get_post_terms($post->ID, 'categories');
        // GET LAST CATEGORY
        foreach($terms as $term) {
          $name = $term->name;
        }
        echo $name;

        if (class_exists('Dynamic_Featured_Image')) {
            global $dynamic_featured_image;
            $featured_images = $dynamic_featured_image->get_featured_images($postId);
            $images = array(get_the_post_thumbnail_url());
            foreach ($featured_images as $featured) {
                array_push($images, $featured['full']);
            }
        }
        ?>


        {{-- CAROUSEL --}}
        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
          {{-- SLIDERS --}}
          <div class="carousel-inner" role="listbox">
            <?php
            $count = 0;
            foreach ($images as $image) {
              ++$count;
              echo "<div class='carousel-item ";
              if($count == 1) echo 'active';
              echo "'>";
              echo "<img id='awesome' class='d-block w-100 img-carousel' src='$image' alt='$count slide'>";
              echo "</div>";
            }
            ?>
          </div>
          {{-- SLIDERS --}}
          {{-- CONTROLS --}}
          <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          {{-- CONTROLS --}}
          <ol class="carousel-indicators">
            <?php
        $target = 0;
        foreach ($images as $image) {
          echo "<li data-target='#carousel-thumb' data-slide-to='$target' class='active'>";
          echo "<img class='d-block w-100' src='$image' class='img-fluid'>";
          echo "</li>";
          ++$target;
        }
        ?>
          </ol>
        </div>
        {{-- CAROUSEL --}}

        <?php
        echo "<div class='py-2 the-excerpt'>";
        the_excerpt();
        echo "</div>";
        echo "<div class='row'>";
        $terms = wp_get_post_terms($post->ID, 'features');
        $count = 0;
        foreach ($terms as $term) {
            ++$count;
            if ($count < 4) {
                echo "<div class='col' style='text-align: center;'>";
                echo "<img src='".get_field('feature_image', $term)['url']."' style='width: 50px;' class='mr-1'><br>";
                echo $term->name . "<br></div>";
            }
        }
        echo "</div>";
        ?>

        @include('partials/sections/accordion')
      </div>
      {{-- RIGHT SIDE  --}}
      <div class="col-md-5">
        <form method="POST">
          <input type="hidden" name="title" value="{!! get_the_title() !!}">

          <div class="custom-panel mb-3">
            <div class="row" id="front-tire">

              <div class="col-md-3">
                <img src="@asset('images/fronttire.png')">
              </div>

              <div class="col-md-9">
                <select class="custom-select mb-1" name='front-size' onchange="changeSpecs()">
                  <?php
                  $field       = get_field_object('tyre_sizes', $post->ID);
                  $front_sizes = explode(', ', $field['value']);
                  foreach ($front_sizes as $front_size) {
                      echo "<option ";
                      if($_POST['front-size'] && $_POST['front-size'] == $front_size) echo 'selected';
                      echo ">$front_size</option>";
                  }
                    ?>
                </select>
              </div>

              <!-- <div class="col-md-3">
                <select class="custom-select mb-1" name="front-size-quantity">
                  <?php
                  // $c = 0;
                  // while ($c != 7) {
                  //     echo "<option ";
                  //     if($_POST['front-size-quantity'] && $_POST['front-size-quantity'] == $c) echo 'selected';
                  //     echo ">$c</option>";
                  //     ++$c;
                  // }
                   ?>
                </select> -->
              <!-- </div> -->

            </div>
            <div class="row mt-3" id="back-tire">

              <div class="col-md-3">
                <img src="@asset('images/backtire.png')">
              </div>

              <div class="col-md-9">
                <select class="custom-select mb-1" name="rear-size" onchange="changeSpecs()">
                  <?php
                  $field      = get_field_object('tyre_sizes', $post->ID);
                  $rear_sizes = explode(', ', $field['value']);

                  foreach ($rear_sizes as $rear_size) {
                    echo "<option ";
                    if($_POST['rear-size'] && $_POST['rear-size'] == $rear_size) echo 'selected';
                    echo ">$rear_size</option>";
                  }
                    ?>
                </select>
              </div>

              <!-- <div class="col-md-3">
                <select class="custom-select mb-1" name="rear-size-quantity">
                  <?php
                  // $c = 0;
                  // while ($c != 7) {
                  //   echo "<option ";
                  //   if($_POST['rear-size-quantity'] && $_POST['rear-size-quantity'] == $c) echo 'selected';
                  //   echo ">$c</option>";
                  //   ++$c;
                  // }
                   ?>
                </select>
              </div> -->

            </div>

            <p id="information" class="mt-2">
              <span class="fa-stack fa-xs primary-color" data-toggle="tooltip" data-placement="top" title="Tyre construction must match on a vehicle axle. This only specifically prohibits mixing of bias and radial tyres on an axle.">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-info fa-stack-1x fa-inverse"></i>
              </span>
              <small>Why 2 different sizes?</small>
            </p>

            <!-- <hr> -->

            <!-- <p id="store-availability" class="mt-2">
                <i class="fas fa-map-marker-alt"></i> Select a store
                <div id="availability"></div>
                <select name="storename" class="custom-select my-1" id="find-store" onchange="tyreAvailability()">
                  <option selected disabled>Tyre is available on the stores listed</option>
                  <?php
                  // $store_field  = get_field_object('stores', $post->ID);
                  // $stores       = explode(', ', $store_field['value']);
                  // foreach($stores as $store){
                  //   echo "<option ";
                  //   if($_POST['storename'] && $_POST['storename'] == $store) echo 'selected';
                  //   echo ">$store</option>";
                  // }
                  ?>

                </select>
            </p> -->

            <!-- <hr>
            <p><button type="submit" name="request_price" class="btn btn-secondary btn-lg btn-block">REQUEST A PRICE</button></p>
            <p><button type="submit" name="book_fitting" class="btn btn-primary btn-lg btn-block">BOOK A FITTING</button></a></p> -->
          </div>

          <!-- <div class="dotted-panel mb-3">
            <?php
          //  dynamic_sidebar('contact_us_tyre');
           ?>
          </div> -->
        </form>

        <?php
        if (isset($_POST['request_price'])) {
            echo "<div class='custom-panel mb-5'>";
            echo do_shortcode("[caldera_form id='CF5e27ae7d1575e']");
            echo "</div>";
        }
        if (isset($_POST['book_fitting'])) {
          echo "<div class='custom-panel mb-5'>";
          echo do_shortcode("[caldera_form id='CF5e27ae8c23b10']");
          echo "</div>";
        }
        ?>
      </div>
    </div>

  </div>
  </article>
  <script>
    jQuery(function() {
      jQuery('[data-toggle="tooltip"]').tooltip()
      jQuery('.carousel').carousel('pause');
      magnify("awesome", 5);
      changeSpecs();
    })
  </script>
