<?php
$type = 'latest-news';
$link = get_post_permalink();
?>
<div class="col-md-3">
  <article class="mb-3 post-<?=get_the_ID()?> <?=($type)?>">

    <a href="<?=get_the_permalink()?>">
      <?=(the_post_thumbnail('full',[
                'class' => 'img-fluid w-100'
            ]
            ))?>
    </a>
    <div class="pt-3 pb-5 px-4">
      <div class="content" style="min-height: 5em;">
        <h3>
          <a href="<?=get_the_permalink()?>" class="color-secondary-dark"><?=(get_the_title())?></a>
        </h3>
      </div>

      <div class="features" style="min-height: 5em;">
        <?php
        $terms = wp_get_post_terms( $post->ID, 'features' );
        $count = 0;
        foreach($terms as $term) {
          ++$count;
          if($count < 4) {
            echo $term->name . "<br>";
          }
        }
        ?>
      </div>

      <?php if(!empty($link[0])) : ?>
      <div class="more">
        <a href="<?=get_the_permalink()?>">
          <button type="button" class="btn btn-secondary btn-block">MORE DETAILS</button>
        </a>
      </div>
      <?php endif; ?>
    </div>
  </article>
</div>
