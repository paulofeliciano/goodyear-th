@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  <form class="group" role="search" method="get" action="<?php echo site_url(); ?>/">
    <input type="hidden" name="cat" value="<?php echo $categoria_id; ?>" />

    <!-- General Search -->
    <input type="text" value="General" name="s" onfocus="if (this.value == 'General') {this.value = '';}" onblur="if (this.value == '') {this.value = 'General';}" />
    <!-- Tag Search -->
    <input type="text" value="Etiqueta" name="tags" onfocus="if (this.value == 'Etiqueta') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Etiqueta';}" />
    <!-- Autor Search -->
    <input type="text" value="Autor" name="autor" onfocus="if (this.value == 'Autor') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Autor';}" />

    <input value="Buscar" type="submit">
</form>

  <div class="row pt-5">
    <?php
    while (have_posts()) {
      the_post();
      ?>
      @include('partials.content-'.get_post_type())
      <?php
    }
    ?>

  </div>
{{ the_posts_pagination() }}
<br>
@endsection
