@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  <h1 class="mt-5">PROMOS</h1>
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  <div class="row pt-5">
    <?php
    while (have_posts()) {
      the_post();
      ?>
      @include('partials.content-'.get_post_type())
      <?php
    }
    ?>

  </div>
  <div class="mb-4">
      {{ the_posts_pagination() }}
  </div>
@endsection
